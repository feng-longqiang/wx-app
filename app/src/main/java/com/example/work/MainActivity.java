package com.example.work;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;
public class MainActivity extends AppCompatActivity implements LinearLayout.OnClickListener{



        protected void onCreate(Bundle savedInstanceState) {

        setContentView(R.layout.activity_main);
        getSupportActionBar().hide();
        super.onCreate(savedInstanceState);
        LinearLayout L1=findViewById(R.id.part1);
        LinearLayout L2=findViewById(R.id.part2);
        LinearLayout L3=findViewById(R.id.part3);
        LinearLayout L4=findViewById(R.id.part4);
        ImageView V1=findViewById(R.id.imageView1);
        ImageView V2=findViewById(R.id.imageView2);
        ImageView V3=findViewById(R.id.imageView3);
        ImageView V4=findViewById(R.id.imageView4);
        L1.setOnClickListener(this);
        L2.setOnClickListener(this);
        L3.setOnClickListener(this);
        L4.setOnClickListener(this);
        V2.setImageResource(R.drawable.txlp);
        V3.setImageResource(R.drawable.fxp);
        V4.setImageResource(R.drawable.wp);
        V1.setImageResource(R.drawable.wxn);
        replace(new BlankFragment());

    }


    @Override
    public void onClick(View view) {
        TextView T=findViewById(R.id.textView);
            switch (view.getId()){
                case R.id.part1:
                    replace(new BlankFragment());change(0);T.setText("微信");
                    break;
                case R.id.part2:
                    replace(new BlankFragment2());change(1);T.setText("通讯录");
                    break;
                case R.id.part3:
                    replace(new BlankFragment3());change(2);T.setText("发现");
                    break;
                case R.id.part4:
                    replace(new BlankFragment4());change(3);T.setText("我");
                    break;
                    default:break;

            }

    }
    private void replace(Fragment fragment){
        FragmentManager fragmentManager=getSupportFragmentManager();
        FragmentTransaction transaction=fragmentManager.beginTransaction();
        transaction.replace(R.id.frametext,fragment);
        transaction.commit();
    }

    private  void change(int i) {
        ImageView V1=findViewById(R.id.imageView1);
        ImageView V2=findViewById(R.id.imageView2);
        ImageView V3=findViewById(R.id.imageView3);
        ImageView V4=findViewById(R.id.imageView4);
        TextView T1=findViewById(R.id.textView1);
        TextView T2=findViewById(R.id.textView2);
        TextView T3=findViewById(R.id.textView3);
        TextView T4=findViewById(R.id.textView4);
            switch(i)
            {
                case 0:V2.setImageResource(R.drawable.txlp);
                        V3.setImageResource(R.drawable.fxp);
                        V4.setImageResource(R.drawable.wp);
                        V1.setImageResource(R.drawable.wxn);


                    break;
                case 1:
                    V2.setImageResource(R.drawable.txln);
                    V3.setImageResource(R.drawable.fxp);
                    V4.setImageResource(R.drawable.wp);
                    V1.setImageResource(R.drawable.wxp);
                    break;
                case 2:
                    V2.setImageResource(R.drawable.txlp);
                    V3.setImageResource(R.drawable.fxn);
                    V4.setImageResource(R.drawable.wp);
                    V1.setImageResource(R.drawable.wxp);
                    break;
                case 3:
                    V2.setImageResource(R.drawable.txlp);
                    V3.setImageResource(R.drawable.fxp);
                    V4.setImageResource(R.drawable.wn);
                    V1.setImageResource(R.drawable.wxp);
                    break;
                    default:break;
            }
        ;

    }

}
